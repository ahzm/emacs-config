(setq cider-cljs-lein-repl "(do (use 'figwheel-sidecar.repl-api) (start-figwheel!) (cljs-repl))")

(with-eval-after-load 'company
  (company-flx-mode +1))
