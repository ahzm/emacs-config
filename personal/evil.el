(require 'evil-leader)
(global-evil-leader-mode)

(evil-leader-mode 1)
(evil-leader/set-leader "SPC")
(evil-escape-mode 1)

(setq-default evil-escape-key-sequence "; ")
(setq-default evil-escape-unordered-key-sequence 1)


(evil-leader/set-key
  "o" 'other-window
  "b" 'ido-switch-buffer
  "," 'evil-buffer
  "k" 'kill-buffer
  "m" 'eshell
  ";" 'evil-escape
  "q" 'avy-goto-word-1
  "t" 'avy-goto-char
  "l" 'avy-goto-line
  "g" 'magit-status)


(eval-after-load "evil"
  '(progn
     (define-key evil-normal-state-map "-" 'delete-other-windows)
     (define-key evil-normal-state-map "\\" 'evil-repeat-find-char-reverse)
     (define-key evil-normal-state-map "H" 'evil-first-non-blank)
     (define-key evil-normal-state-map "Y" 'copy-to-end-of-line)
     (define-key evil-normal-state-map "L" 'evil-last-non-blank)
     (define-key evil-normal-state-map (kbd "C-c f") 'flymake-display-warning)
     (define-key evil-insert-state-map (kbd "C-d") 'delete-forward-char)
     (define-key evil-insert-state-map (kbd "C-h") 'backward-delete-char)))
